#include <stdio.h>

int main() {
	int x = 10;
	int *y = &x;
	int v[3] = {4, 6, 9};
	printf("%p\n", &x);
	printf("%d\n", *y);
	y = v;
	for (int i = 0; i < 3; i++ ) {
		printf("Value of *ptr = %d\n", *y);
		printf("Value of y = %p\n\n", y);
		printf("Value of v[%d] = %p\n\n",i, &v[i]); 
		y++;
	}
	return 0;
}
