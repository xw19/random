#include <stdio.h>

void move(int n, int source, int dest, int aux) {
	if ( n==1) {
		printf("Move disks 1 from rod %d to rod %d\n", source, dest);
		return;
	}
	move(n-1, source, aux, dest);
	printf("Move disks %d from rod %d to rod %d\n", n, source, dest);
	move(n-1, aux, dest, source);
}

int main(){
	int n = 4;
	move(n, 1, 3, 2);
	return 0;
}
